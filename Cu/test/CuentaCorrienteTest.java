/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pacojurado
 */
public class CuentaCorrienteTest {

    CuentaCorriente instance;
    static CuentaCorriente destino;

    public CuentaCorrienteTest() {
    }

    @BeforeClass //Se ejecuta antes de la primera prueba
    public static void setUpClass() {
        destino = new CuentaCorriente(2000);
    }

    @AfterClass // Se ejecuta despues de la ultima prueba
    public static void tearDownClass() {
    }

    @Before //Se ejecutan antes de cada prueba
    public void setUp() {

        instance = new CuentaCorriente(1000);

    }

    @After  // Se ejecutan despues de cada prueba
    public void tearDown() {
    }

    /**
     * Test of ingresa method, of class CuentaCorriente.
     */
    @Test //Se ejecuta y prueba algo, cada test es una prueba
    public void testIngresa() {
        System.out.println("ingresa");
        double i = 200;
        double saldoInicial = instance.getSaldo();
        instance.ingresa(i);
        double saldoFinal = instance.getSaldo();
        assertTrue(saldoFinal == saldoInicial + i);

        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        //PROBAMOS EL MeTODO INGRESAR
    }

    @Test //Se ejecuta y prueba algo, cada test es una prueba
    public void testIngresa2() {

        System.out.println("ingresa");
        double i = -200; //Va a salir una excepcion
        double saldoInicial = instance.getSaldo();
        double saldoFinal = 0;

        try {

            instance.ingresa(i);
            System.out.println("Por aqui no puede pasar");

        } catch (Exception e) {
            System.out.println("Excepcion en ingresa2: " + e.getMessage());
            saldoFinal = instance.getSaldo();
            assertTrue(saldoFinal == saldoInicial);
        }

        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        //PROBAMOS EL MeTODO INGRESAR
    }
    
    @Test //Se ejecuta y prueba algo, cada test es una prueba
    public void testIngresaMaximo() {

        System.out.println("ingresa");
        double i = 100 + CuentaCorriente.MAXIMO_OPERACION; //Va a salir una excepcion
        double saldoInicial = instance.getSaldo();
        double saldoFinal = 0;

        try {

            instance.ingresa(i);
            System.out.println("Por aqui no puede pasar");

        } catch (Exception iM) {
            
            System.out.println("Excepcion en IngresaMaximo: " + iM.getMessage());
            saldoFinal = instance.getSaldo();
            assertTrue(saldoFinal == saldoInicial);
        }

        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        //PROBAMOS EL MeTODO INGRESAR
    }

    /**
     * Test of extrae method, of class CuentaCorriente.
     */
    @Test
    public void testExtrae() {

        System.out.println("extrae");
        double e = 200;
        double saldoInicial = instance.getSaldo();
        instance.extrae(e);
        double saldoFinal = instance.getSaldo();
        assertTrue(saldoFinal == saldoInicial - e);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    
    
    /**
     * Test of extrae method, of class CuentaCorriente.
     */
    @Test
    public void testExtrae2() {
        System.out.println("extrae");
        double e = -200;
        double saldoInicial = instance.getSaldo();
        double saldoFinal;

        try {

            instance.extrae(e);

        } catch (Exception e2) {
            System.out.println("Excepción en extrae2: " + e2.getMessage());
            saldoFinal = instance.getSaldo();
            assertTrue(saldoFinal == saldoInicial);

        }

        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of tranferir method, of class CuentaCorriente.
     */
    @Test
    public void testTranferir() {
        System.out.println("tranferir");
        double cantidad = 200;
        double saldoInicialC = instance.getSaldo();
        double saldoInicialD = destino.getSaldo();
        instance.tranferir(destino, cantidad);
        double saldoFinalC = instance.getSaldo();
        double saldoFinalD = destino.getSaldo();
        assertTrue(saldoFinalD == saldoInicialD + cantidad && saldoFinalC == saldoInicialC - cantidad);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of tranferir method, of class CuentaCorriente.
     */
    @Test
    public void testTranferirM() {

        System.out.println("tranferir");
        double cantidad = 200 + CuentaCorriente.MAXIMO_OPERACION;
        double saldoInicialC = instance.getSaldo();
        double saldoInicialD = destino.getSaldo();

        try {

            instance.tranferir(destino, cantidad);

        } catch (Exception tM) {

            System.out.println("Excepcion en TransferirM: " + tM.getMessage());
            double saldoFinalC = instance.getSaldo();
            double saldoFinalD = destino.getSaldo();
            assertTrue(saldoFinalD == saldoInicialD && saldoFinalC == saldoInicialC);

        }
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test of tranferir method, of class CuentaCorriente.
     */
    @Test
    public void testTranferirN() {

        System.out.println("tranferir");
        double cantidad = -500;
        double saldoInicialC = instance.getSaldo();
        double saldoInicialD = destino.getSaldo();

        try {

            instance.tranferir(destino, cantidad);

        } catch (Exception tN) {

            System.out.println("Excepcion en TransferirN: " + tN.getMessage());
            double saldoFinalC = instance.getSaldo();
            double saldoFinalD = destino.getSaldo();
            assertTrue(saldoFinalD == saldoInicialD && saldoFinalC == saldoInicialC);

        }
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of estaAlDescubierto method, of class CuentaCorriente.
     */
    @Test
    public void testEstaAlDescubierto() {

        System.out.println("estaAlDescubierto");
        boolean descubierto = false;
        double cantidad = 1500;
        instance.getSaldo();
        instance.extrae(cantidad);
        boolean estado = instance.estaAlDescubierto();

        if (instance.getSaldo() < 0) {

            descubierto = true;
        }

        assertEquals(descubierto, estado);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testSuperaDescubierto() {

        System.out.println("estaAlDescubierto");
        boolean descubierto = false;
        double cantidad = 2500;

        instance.getSaldo();

        try {

            instance.extrae(cantidad);

        } catch (Exception S) {

            System.out.println("Excepción en SuperaDescubierto " + S.getMessage());

        }

        boolean estado = instance.estaAlDescubierto();

        if (instance.getSaldo() < 0) {

            descubierto = true;

        }

        assertEquals(descubierto, estado);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");

    }
    
    

}
